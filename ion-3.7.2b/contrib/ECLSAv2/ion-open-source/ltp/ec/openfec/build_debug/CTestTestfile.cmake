# CMake generated Testfile for 
# Source directory: /home/bisa/git/ECLSA/ion-3.7.2b/contrib/ECLSAv2/ion-open-source/ltp/ec/openfec
# Build directory: /home/bisa/git/ECLSA/ion-3.7.2b/contrib/ECLSAv2/ion-open-source/ltp/ec/openfec/build_debug
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("src")
subdirs("applis/eperftool")
subdirs("applis/howto_examples/simple_client_server")
subdirs("tools/descr_stats_v1.2")
subdirs("tests")
